var express = require('express');
var app = express();
const fs = require('fs');
const path = require('path');
var router = express.Router();
const gamesFolder = path.join(__dirname, "games");

app.set('view engine', 'pug');

app.use("/static/unity-gamejam-2018/initial-version", express.static(__dirname + "/games/unity-gamejam-2018/initial-version"));

// index
app.get('/', sendGamesList);
app.get('/games', sendGamesList);
app.get('/games/:gameName/', sendGameVersions);
app.get('/games/:gameName/:gameVersion/', sendGame);


// Fallback
app.use(function (req, res) {
  res.redirect('/');
});


app.listen(3000, function () {
  console.log('Listening 3000...');
});

function sendGame(req, res) {
  const gameName = req.params.gameName;
  const gameVersion = req.params.gameVersion;
  const gameWebGLHtml = path.join(gamesFolder, gameName, gameVersion, "index.html");
  fs.access(gameWebGLHtml, (err) => {
    if (err) {
      res.render('error', { error: `Version "${gameVersion}" of the game "${gameName}" does not exist!` });
      return;
    }
    res.render('game-version-preview', { gameName, gameVersion });
  });
}

function sendGameVersions(req, res) {
  const gameName = req.params.gameName;
  const gameFolder = path.join(gamesFolder, gameName);
  fs.readdir(gameFolder, (err, gameVersions) => {
    if (err) {
      res.render('error', { error: `"${gameName}" Such game does not exist!`});
      return;
    }
    res.render('game-versions-list', { gameName, gameVersions });
  });
}


function sendGamesList(req, res) {
  fs.readdir(gamesFolder, (err, folders) => {
    if (err) {
      res.render('error');
      return;
    }
    res.render('index', { folders });
  });
}