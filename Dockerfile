FROM node:10.13-alpine

WORKDIR /home/node/app

# Install deps
COPY ./package* ./
RUN npm install

COPY . .

# Expose ports (for orchestrators and dynamic reverse proxies)
EXPOSE 3000

# Start the app
CMD npm start